import React, { Component } from 'react';
 const Square = ({whenClicked = () => {console.log("no op")}, caption}) => {
    return <div className="game-box" onClick={whenClicked}>{caption}</div>
}

export default Square