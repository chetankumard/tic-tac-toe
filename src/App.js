import React, { Component } from 'react';
// import Square from "./Components/Square"
// import math, { matrix } from 'mathjs'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import logo from './logo.svg';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props)
    this.users = ["a", "b"]
    this.initialGameBoard = [[{ user: null, citiation: null }, { user: null, citiation: null }, { user: null, citiation: null }], [{ user: null, citiation: null }, { user: null, citiation: null }, { user: null, citiation: null }], [{ user: null, citiation: null }, { user: null, citiation: null }, { user: null, citiation: null }]]
    this.state = {
      gameboard: this.initialGameBoard,
      currentUser: this.users[0],
      moves: 0
    }
    this.citiation = new Map()
    this.citiation.set("a", "X")
    this.citiation.set("b", "O")
  }
  squareClicked(rowIndex, colIndex) {
    console.log(`clicked on col: ${colIndex + 1} and row: ${rowIndex + 1}`)
    this.setState({ moves: this.state.moves + 1 })

    if (this.state.gameboard[rowIndex][colIndex].user != null) return

    const newBoard = this.state.gameboard.map((outer, ri) => {
      if (ri === rowIndex) {
        return outer.map((inner, ci) => {
          if (ci === colIndex) {
            return { ...inner, user: this.state.currentUser, citiation: this.citiation.get(this.state.currentUser) }
          } else {
            return inner
          }
        })
      } else {
        return outer
      }
    })
    console.log("moves: ", this.state.moves)

    this.setState({ gameboard: newBoard }, () => {
      this.setState({ currentUser: this.users.find(s => s !== this.state.currentUser) })
      if (this.state.moves > 3) {
        this.checkIfGameOver()
      }
    })
  }
  reset() {
    this.setState({ gameboard: this.initialGameBoard, moves: 0, currentUser: this.users[0] , message: null}, () => {
      console.log("RESET")
    })
  }
  checkIfGameOver() {
    const gmR = this.state.gameboard;
    let diag = []
    let diag2 = []
    for (let i = 0; i < gmR.length; i++) {
      let rowUsers = []
      let colUsers = []

      for (let j = 0; j < gmR.length; j++) {
        rowUsers.push(gmR[i][j].user);
        colUsers.push(gmR[j][i].user);
        if (i == j) {
          console.log(i, j)
          diag.push(gmR[i][j].user)
        }

        if (i === 0 && j === gmR.length - 1) {
          console.log(i, j)
          diag2.push(gmR[i][j].user)
        }
        if (i === 1 && j === gmR.length - 2) {
          console.log(i, j)
          diag2.push(gmR[i][j].user)
        }
        if (i === 2 && j === gmR.length - 3) {
          console.log(i, j)
          diag2.push(gmR[i][j].user)
        }

      }
      let nonNullUsers = rowUsers.filter(s => s !== null)
      if (nonNullUsers.length == 3 && nonNullUsers.every(v => v === nonNullUsers[0])) {
        this.setState({message: `user ${this.state.currentUser} wins`})
        setTimeout(() => { this.reset() }, 1000)
      }
      let colNonNull = colUsers.filter(s => s !== null)
      if (colNonNull.length == 3 && colNonNull.every(v => v === colNonNull[0])) {
        this.setState({message: `user ${this.state.currentUser} wins`})
        setTimeout(() => { this.reset() }, 1000)
      }
    }
    let diagNonulls = diag.filter(s => s !== null)
    if (diagNonulls.length == 3 && diagNonulls.every(v => v === diagNonulls[0])) {
      this.setState({message: `user ${this.state.currentUser} wins`})
      setTimeout(() => { this.reset() }, 1000)
    }

    let diag2Nonulls = diag2.filter(s => s !== null)
    console.log(diag2)
    if (diag2Nonulls.length == 3 && diag2Nonulls.every(v => v === diag2Nonulls[0])) {
      this.setState({message: `user ${this.state.currentUser} wins`})
      setTimeout(() => { this.reset() }, 1000)
    }

  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to my tic tac toe</h2>
        </div>
        <p className="App-intro">
         {(this.state.message || `Now playing User: ${this.state.currentUser}`).toLocaleUpperCase()}
        </p>
        {
          this.state.gameboard.map((outer, rowIndex) => (
            <div className="game-board">
              {
                outer.map((inner, colIndex) => (
                  <React.Fragment>
                    <svg width="300" height="100" onClick={() => { this.squareClicked(rowIndex, colIndex) }}>
                      <rect width="300" height="100" className="rect-svg" />
                      <text x="150" y="70" font-family="Verdana" font-size="35" fill="white">{(inner.citiation || "")}</text>
                    </svg>
                  </React.Fragment>
                ))
              }
            </div>
          ))
        }
      </div>
    );
  }
}

export default App;
